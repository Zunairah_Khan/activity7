package com.example.classactivity7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    ImageButton loc;
    DatePicker dp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        loc = (ImageButton) findViewById(R.id.imageButton);
        dp = (DatePicker) findViewById(R.id.simpledatepicker);

        String day = "Day = " + dp.getDayOfMonth();
        String month = "Month = " + (dp.getMonth()+1);
        String year = "Year = " + dp.getYear();

        Toast.makeText(getApplicationContext(), day + '\n' + month + '\n' + year, Toast.LENGTH_LONG).show();

        loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LocationActivity.class);
                startActivity(intent);
            }
        });

    }
}