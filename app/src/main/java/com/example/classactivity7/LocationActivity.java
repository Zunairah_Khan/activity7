package com.example.classactivity7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class LocationActivity extends AppCompatActivity {

    private GpsTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        TextView name=findViewById(R.id.name);
        TextView loc=findViewById(R.id.loc);


        SharedPreferences prefs=getSharedPreferences("MySharedPref",MODE_PRIVATE);
        name.setText("Name: "+prefs.getString("username",""));

        gpsTracker = new GpsTracker(LocationActivity.this);
        if(gpsTracker.canGetLocation()){
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();

//            Toast.makeText(getApplicationContext(), String.format("Latitude = %.2f", latitude), Toast.LENGTH_LONG).show();
//            Toast.makeText(getApplicationContext(), String.format("Longitude = %.2f", longitude), Toast.LENGTH_LONG).show();

            loc.setText("Latitude: "+latitude+" \nLongitude: "+longitude+"\n");
        }
        else{
            gpsTracker.showSettingsAlert();
        }
    }
}